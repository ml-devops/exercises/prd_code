""""
Try/Except examples for ML DevOps workshop



"""


def divide_vals(numerator, denominator):
    """
        Args:
            numerator: (float) numerator of fraction
            denominator: (float) denominator of fraction

        Returns:
            fraction_val: (float) numerator/denominator
    """

    try:
        fraction = float(numerator / denominator)
        return fraction
    except ZeroDivisionError:
        return "Denominator is zero, we might find a limit"


def num_words(text):
    """
        Args:
            text: (string) string of words



        Returns:
            num_words: (int) number of words in the string
    """
    try:
        words = len(text.split(' '))
        return words

    except TypeError:
        return "input must be string"
